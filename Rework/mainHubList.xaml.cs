//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
//
//*********************************************************

using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using SDKTemplate;
using System;
using Windows.Foundation;
using Expression.Blend.SampleData.SampleDataSource;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace SDKTemplate
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class mainHubList : SDKTemplate.Common.LayoutAwarePage
    {
        // A pointer back to the main page.  This is needed if you want to call methods in MainPage such
        // as NotifyUser()

        // holds the sample data - See SampleData folder
        StoreData storeData = null;
        private Geolocator _geolocator = null;
        public static Geoposition pos = null;
        async private Task<Geoposition> GetGeolocation()
        {
            Geoposition pos = await _geolocator.GetGeopositionAsync();
            return pos;
        }

        public mainHubList()
        {
            this.InitializeComponent();
            setupLayout();
        }


        private async void setupLayout()
        {
            // create a new instance of store data
            if (_geolocator == null)
            {
                try {
                _geolocator = new Geolocator();
                pos = await GetGeolocation();
                }
                catch (Exception)
                {
                    throw new NotSupportedException( "This machine does not support the Windows Geolocation API." );
                }
            }



            var point = pos.Coordinate.Point.Position;

            storeData = new StoreData();


            bool delay1 = false;
            String ListSearch = "Pizza Food";

            if (App.itemListSortedListings.ContainsKey(ListSearch) == false)
                delay1 = true;

            storeData.setupItems(ListSearch, ListSearch, "cZ" + point.Longitude + "," + point.Latitude);
            ItemListView.ItemsSource = StoreData.itemList[ListSearch];


            ListSearch = "Burgers";

            if (App.itemListSortedListings.ContainsKey(ListSearch) == false)
                delay1 = true;

//           if (delay1 == true)
//               await Task.Delay(1200);

            storeData.setupItems(ListSearch, ListSearch, "cZ" + point.Longitude + "," + point.Latitude);
            ItemListViewBurgers.ItemsSource = StoreData.itemList[ListSearch];
            ListSearch = "Sushi Food";

            if (App.itemListSortedListings.ContainsKey(ListSearch) == false)
                delay1 = true;

//            if (delay1 == true)
            if (delay1 == true)
                await Task.Delay(1200);

            storeData.setupItems(ListSearch, ListSearch, "cZ" + point.Longitude + "," + point.Latitude);
            ItemListViewSushi.ItemsSource = StoreData.itemList[ListSearch];

            ListSearch = "Chinese Food";

            if (App.itemListSortedListings.ContainsKey(ListSearch) == false)
                delay1 = true;


//            if (delay1 == true)
            storeData.setupItems(ListSearch, ListSearch, "cZ" + point.Longitude + "," + point.Latitude);
            ItemListViewChinese.ItemsSource = StoreData.itemList[ListSearch];

            if (App.favorites.Count >= 1)
            {
                ListSearch = "Favourites";

                storeData.setupItems(ListSearch, ListSearch, null);
                ItemListViewFavs.ItemsSource = StoreData.itemList[ListSearch];
            }

        }

        /// <summary>
        /// We will visualize the data item in asynchronously in multiple phases for improved panning user experience 
        /// of large lists.  In this sample scneario, we will visualize different parts of the data item
        /// in the following order:
        /// 
        ///     1) Placeholders (visualized synchronously - Phase 0)
        ///     2) Tilte (visualized asynchronously - Phase 1)
        ///     3) Category and Image (visualized asynchronously - Phase 2)
        ///
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        void ItemListView_ContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            ItemViewer iv = args.ItemContainer.ContentTemplateRoot as ItemViewer;

            if (args.InRecycleQueue == true)
            {
                iv.ClearData();
            }
            else if (args.Phase == 0)
            {
                iv.ShowPlaceholder(args.Item as Item);

                // Register for async callback to visualize Title asynchronously
                args.RegisterUpdateCallback(ContainerContentChangingDelegate);
            }
            else if (args.Phase == 1)
            {
                iv.ShowTitle();
                args.RegisterUpdateCallback(ContainerContentChangingDelegate);
            }
            else if (args.Phase == 2)
            {
                iv.ShowDistance();
                iv.ShowImage();
            }

            // For imporved performance, set Handled to true since app is visualizing the data item
            args.Handled = true;
        }



        private void ItemClickHandler(object sender, ItemClickEventArgs e)
        {
            Item item = e.ClickedItem as Item;

            MainPage.Current.Frame.Navigate(typeof(InformationPage), item.ID + ";" + item.Title + ";" + item.City + ";" + item.Prov + ";" + item.Distance);
        }
        /// <summary>
        /// Managing delegate creation to ensure we instantiate a single instance for 
        /// optimal performance. 
        /// </summary>
        private TypedEventHandler<ListViewBase, ContainerContentChangingEventArgs> ContainerContentChangingDelegate
        {
            get
            {
                if (_delegate == null)
                {
                    _delegate = new TypedEventHandler<ListViewBase, ContainerContentChangingEventArgs>(ItemListView_ContainerContentChanging);
                }
                return _delegate;
            }
        }
        private TypedEventHandler<ListViewBase, ContainerContentChangingEventArgs> _delegate;

    }
}
