﻿// Copyright (c) Microsoft. All rights reserved.
using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using SDKTemplate;
using System.Collections.Generic;
using Windows.Storage;
using System.Text;
using System.Threading.Tasks;
using Expression.Blend.SampleData.SampleDataSource;

namespace SDKTemplate
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App : Application
    {
        /// <summary>
        /// Initializes the singleton Application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
       //http://www.yellowapi.com/wp-content/Images/places_api_ic.png /// </summary>
        /// 

        public static StorageFolder infoFolder = null;
        public static List<Item> favorites = new List<Item>();
        public static string favsFile = "favorites.txt";
        public static List<String> usableKeys = new List<String>();

        //public static String APIKey = "d4h9eb4cppjtkaw5kmkjqxfs";
        //public static String APIKey = "u55kvbj2rejxu5dcezj8wjua";

        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            infoFolder = ApplicationData.Current.LocalFolder;
            usableKeys.Clear();
            usableKeys.Add("d4h9eb4cppjtkaw5kmkjqxfs");
            usableKeys.Add("u55kvbj2rejxu5dcezj8wjua");
           readFavs();
        }

        public static Boolean isFaved(String ID) {
            foreach (Item item in favorites) {
                System.Diagnostics.Debug.WriteLine("ID: Got: " + item.ID + " Wanted " + ID);
                if (item.ID.ToLower() == ID.ToLower())
                    return true;
            }
            System.Diagnostics.Debug.WriteLine("Gave up on life.");
            return false;
        }

        public static void toggleFav(Item Comp)
        {
            if (isFaved(Comp.ID))
            {
                Item itemToRemove = null;
                foreach (Item item in favorites)
                {
                    if (item.ID.ToLower() == Comp.ID.ToLower())
                        itemToRemove = item;
                }
                if (itemToRemove != null)
                    favorites.Remove(itemToRemove);
            }
            else
                favorites.Add(Comp);
            saveFavs();
        }



        public static async void saveFavs()
        {

            StorageFile file = await infoFolder.CreateFileAsync(favsFile, CreationCollisionOption.ReplaceExisting);
            StringBuilder builder = new StringBuilder();
            foreach (Item compID in favorites){
                if (compID.ID != null && compID.ID != "")
                builder.Append("ID:" + compID.ID + "ҀName:" + compID.Title + "ҀDistance:" + compID.Distance + "ҀCity:" + compID.City + "ҀProv:" + compID.Prov + "" + "Ҁ|");
            }


            await FileIO.WriteTextAsync(file, builder.ToString());
        }

        public static async void readFavs()
        {
            try
            {
                StorageFile file = await infoFolder.GetFileAsync(favsFile);
                if (!file.IsAvailable)
                file = await infoFolder.CreateFileAsync(favsFile, Windows.Storage.CreationCollisionOption.GenerateUniqueName);
                    string text = await FileIO.ReadTextAsync(file);
                    String[] splitUpText = text.Split('|');
                    favorites.Clear();
                    foreach(String CompID in splitUpText){
                        if (CompID != null && CompID != "")
                        {
                            Item item = new Item();
                            String[] subInfo = CompID.Split('Ҁ');
                            foreach(String subInfoSet in subInfo){
                                String[] splitupInfo = subInfoSet.Split(':');
                                if (splitupInfo[0].ToLower() == "id")
                                    item.ID = splitupInfo[1];
                                else if (splitupInfo[0].ToLower() == "name")
                                    item.Title = splitupInfo[1];
                                else if (splitupInfo[0].ToLower() == "distance")
                                    item.Distance = splitupInfo[1];
                                else if (splitupInfo[0].ToLower() == "city")
                                    item.City = splitupInfo[1];
                                else if (splitupInfo[0].ToLower() == "prov")
                                    item.Prov = splitupInfo[1];
                            }
                            favorites.Add(item);
                    }
                }
            }
            catch (Exception)
            {

            }



        }
        public static Dictionary<String, String> itemListSortedListings = new Dictionary<String, String>();

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs args)
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();
                // Associate the frame with a SuspensionManager key                                
                SuspensionManager.RegisterFrame(rootFrame, "AppFrame");

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated)
                {
                    // Restore the saved session state only when appropriate
                    try
                    {
                        await SuspensionManager.RestoreAsync();
                    }
                    catch (SuspensionManagerException)
                    {
                        //Something went wrong restoring state.
                        //Assume there is no state and continue
                    }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }
            if (rootFrame.Content == null || !String.IsNullOrEmpty(args.Arguments))
            {
                // When the navigation stack isn't restored or there are launch arguments
                // indicating an alternate launch (e.g.: via toast or secondary tile), 
                // navigate to the appropriate page, configuring the new page by passing required 
                // information as a navigation parameter
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }
            // Ensure the current window is active
            Window.Current.Activate();
        }

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await SuspensionManager.SaveAsync();
            deferral.Complete();
        }


        public async static Task<String> getAPIKey() {
            if (usableKeys.Count >= 1){
                String key = "";
                key = usableKeys[0];
                usableKeys.Remove(key);
                startTimer(key);

                return key;
            }
            else{
                // We have no keys available at this time :(
                await Task.Delay(1600);
                String val = getAPIKey().Result;
                return val;
            }
        }

        private async static void startTimer(string key)
        {
            await Task.Delay(1100);
            usableKeys.Add(key);
        }
    }
}
