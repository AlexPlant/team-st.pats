﻿using Expression.Blend.SampleData.SampleDataSource;
using SDKTemplate;
using SDKTemplate.Common;
// Copyright (c) Microsoft. All rights reserved.
using System;
using System.Collections;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Navigation;
using Windows.UI.ApplicationSettings;
using System.Net.Http;
using Windows.Devices.Geolocation;
using Windows.UI.Popups;
using Newtonsoft.Json;


namespace SDKTemplate
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : SDKTemplate.Common.LayoutAwarePage
    {
        public event System.EventHandler ScenarioLoaded;
        public event EventHandler<MainPageSizeChangedEventArgs> MainPageResized;

        public static MainPage Current;

        private Frame HiddenFrame = null;

        public MainPage()
        {
            this.InitializeComponent();

            // This is a static public property that will allow downstream pages to get 
            // a handle to the MainPage instance in order to call methods that are in this class.
            Current = this;

            // This frame is hidden, meaning it is never shown.  It is simply used to load
            // each scenario page and then pluck out the input and output sections and
            // place them into the UserControls on the main page.
            HiddenFrame = new Windows.UI.Xaml.Controls.Frame();
            HiddenFrame.Visibility = Windows.UI.Xaml.Visibility.Collapsed;
            ContentRoot.Children.Add(HiddenFrame);

            //Add the about flyout page
            SettingsPane.GetForCurrentView().CommandsRequested += MainPage_CommandsRequested;

            // Populate the sample title from the constant in the Constants.cs file.
            Pizza.SelectionChanged += Scenarios_SelectionChanged;
            //Burgers.SelectionChanged += Scenarios_SelectionChanged;
            SizeChanged += MainPage_SizeChanged;
        }

        void MainPage_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs e)
        {
            SettingsCommand AboutCommand =
                new SettingsCommand("AboutPageSettings", "About", (handler) =>
                {
                    AboutPage CreditPage = new AboutPage();
                    CreditPage.Show();
                });

            e.Request.ApplicationCommands.Add(AboutCommand);
        }
        void MainPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            InvalidateSize();
            if (MainPageResized != null)
            {
                MainPageSizeChangedEventArgs args = new MainPageSizeChangedEventArgs();
                args.Width = this.ActualWidth;
                MainPageResized(this, args);
            }
        }
        
        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            PopulateScenarios();
            InvalidateLayout();
                                              
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);

            SettingsPane.GetForCurrentView().CommandsRequested -= MainPage_CommandsRequested;
        }

        private void InvalidateSize()
        {
            // Get the window width
            double windowWidth = this.ActualWidth;
            if (windowWidth != 0.0)
            {


            }
            InvalidateLayout();
        }

        private void InvalidateLayout()
        {

            // Since we don't load the scenario page in the traditional manner (we just pluck out the
            // input and output sections from the page) we need to ensure that any VSM code used
            // by the scenario's input and output sections is fired.
            VisualStateManager.GoToState((Control)OutputSection, "Output" + DetermineVisualState(this.ActualWidth), false);
        }

        private void PopulateScenarios()
        {
            System.Collections.ObjectModel.ObservableCollection<object> ScenarioList = new System.Collections.ObjectModel.ObservableCollection<object>();
            int i = 0;

            // Populate the ListBox with the list of scenarios as defined in Constants.cs.
            
            foreach (Scenario s in scenarios)
            {
                ListBoxItem item = new ListBoxItem();
                s.Title = (++i).ToString() + ") " + s.Title;
                item.Content = s;
                ScenarioList.Add(item);
            }

            // Bind the ListBox to the scenario list.
            Pizza.ItemsSource = ScenarioList;
           
            // Starting scenario is the first or based upon a previous selection.
            int startingScenarioIndex = -1;
            
            if (SuspensionManager.SessionState.ContainsKey("SelectedScenarioIndex"))
            {
                int selectedScenarioIndex = Convert.ToInt32(SuspensionManager.SessionState["SelectedScenarioIndex"]);
                startingScenarioIndex = selectedScenarioIndex;
            }
            Pizza.SelectedIndex = startingScenarioIndex != -1 ? startingScenarioIndex : 0;
            Pizza.ScrollIntoView(Pizza.SelectedItem);


            // Breakage occurs here...
            // Starting scenario is the first or based upon a previous selection.
            /*
            int startingScenarioIndex1 = -1;

            if (SuspensionManager.SessionState.ContainsKey("SelectedScenarioIndex"))
            {
                int selectedScenarioIndex1 = Convert.ToInt32(SuspensionManager.SessionState["SelectedScenarioIndex"]);
                startingScenarioIndex1 = selectedScenarioIndex1;
            }

            Burgers.SelectedIndex = startingScenarioIndex1 != -1 ? startingScenarioIndex1 : 0;
            Burgers.ScrollIntoView(Burgers.SelectedItem);
            */
        }

        /// <summary>
        /// This method is responsible for loading the individual input and output sections for each scenario.  This 
        /// is based on navigating a hidden Frame to the ScenarioX.xaml page and then extracting out the input
        /// and output sections into the respective UserControl on the main page.
        /// </summary>
        /// <param name="scenarioName"></param>
        /// 
        // event handler for when an item is selected within the GridView


        public void LoadScenario(Type scenarioClass)
        {

            // Load the ScenarioX.xaml file into the Frame.
            HiddenFrame.Navigate(scenarioClass, this);

            // Get the top element, the Page, so we can look up the elements
            // that represent the input and output sections of the ScenarioX file.
            Page hiddenPage = HiddenFrame.Content as Page;

            // Get each element.
            UIElement output = hiddenPage.FindName("Output") as UIElement;

            if (output == null)
            {
                // Malformed output section.
                NotifyUser(String.Format(
                    "Cannot load scenario output section for {0}.  Make sure root of output section markup has x:Name of 'Output'",
                    scenarioClass.Name), NotifyType.ErrorMessage);
                return;
            }

            // Find the LayoutRoot which parents the input and output sections in the main page.
            Panel panel = hiddenPage.FindName("LayoutRoot") as Panel;

            if (panel != null)
            {
                // Get rid of the content that is currently in the intput and output sections.
                panel.Children.Remove(output);

                // Populate the input and output sections with the newly loaded content.
                OutputSection.Content = output;
            }
            else
            {
                // Malformed Scenario file.
                NotifyUser(String.Format(
                    "Cannot load scenario: '{0}'.  Make sure root tag in the '{0}' file has an x:Name of 'LayoutRoot'",
                    scenarioClass.Name), NotifyType.ErrorMessage);
            }
        }

        void Scenarios_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (Pizza.SelectedItem != null)
            {
                NotifyUser("", NotifyType.StatusMessage);

                ListBoxItem selectedListBoxItem = Pizza.SelectedItem as ListBoxItem;
                SuspensionManager.SessionState["SelectedScenarioIndex"] = Pizza.SelectedIndex;

                Scenario scenario = selectedListBoxItem.Content as Scenario;
                LoadScenario(scenario.ClassType);
                InvalidateSize();

                // Fire the ScenarioLoaded event since we know that everything is loaded now.
                if (ScenarioLoaded != null)
                {
                    ScenarioLoaded(this, new EventArgs());
                }
            }
            /*
            if (Burgers.SelectedItem != null)
            {
                NotifyUser("", NotifyType.StatusMessage);

                ListBoxItem selectedListBoxItem = Burgers.SelectedItem as ListBoxItem;
                SuspensionManager.SessionState["SelectedScenarioIndex"] = Burgers.SelectedIndex;

                Scenario scenario = selectedListBoxItem.Content as Scenario;
                LoadScenario(scenario.ClassType);
                InvalidateSize();

                // Fire the ScenarioLoaded event since we know that everything is loaded now.
                if (ScenarioLoaded != null)
                {
                    ScenarioLoaded(this, new EventArgs());
                }
            }
             */
        }

        public void NotifyUser(string strMessage, NotifyType type)
        {

        }

        async void Footer_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new Uri(((HyperlinkButton)sender).Tag.ToString()));
        }

        private void submitSearchQuery(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {

            String BusString = args.QueryText;
            preRunMethod(BusString);
        }

        private async void preRunMethod(string BusString)
        {
            String key = await App.getAPIKey();
            runSearch(BusString, key);
        }

        private async void runSearch(string BusString, String key)
        {

            var client = new HttpClient();
            BasicGeoposition point = mainHubList.pos.Coordinate.Point.Position;
            String city = "cZ" + point.Longitude + "," + point.Latitude;
            Uri url = new Uri("http://api.sandbox.yellowapi.com/FindBusiness/?what=" + BusString + "&where=" + city + "&pgLen=500&pg=1&dist=100&fmt=JSON&lang=en&UID=qwdqqdd&apikey=" + key);

            string jsonstring = await client.GetStringAsync(url);
            Businesses.InfoSet values = JsonConvert.DeserializeObject<Businesses.InfoSet>(jsonstring);
            if (values.listings.Count >= 1)
            {
                MainPage.Current.Frame.Navigate(typeof(InformationPage), values.listings[0].id + ";" + values.listings[0].name + ";" + values.listings[0].address.city + ";" + values.listings[0].address.prov + ";" + values.listings[0].distance);
            }
            else
                await new MessageDialog("Failed to find any restaurants with such keywords.").ShowAsync();
        }

    
    }

    public class MainPageSizeChangedEventArgs : EventArgs
    {
        private double width;

        public double Width
        {
            get { return width; }
            set { width = value; }
        }
    }

    public enum NotifyType
    {
        StatusMessage,
        ErrorMessage
    };
}
