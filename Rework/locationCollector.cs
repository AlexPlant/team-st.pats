﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Windows.Devices.Geolocation;

namespace SDKTemplate
{
    class locationCollector
    {

        static Geolocator geo = null;
        static Geoposition pos = null;

        public static async Task<Geoposition> setupLocation()
        {
            if (geo == null)
            {
                geo = new Geolocator();
            }

            pos = await geo.GetGeopositionAsync();

            return pos;


        }
        public static async Task<BasicGeoposition> getLocation (){

            pos = await setupLocation();

            return pos.Coordinate.Point.Position;
        }
    }
}
