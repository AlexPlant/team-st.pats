﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Windows.UI.Xaml.Media;
using System.Net.Http;
using Newtonsoft.Json;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using SDKTemplate;
using Windows.UI.Xaml.Controls;
using SDKTemplate;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using System.Threading;
namespace Expression.Blend.SampleData.SampleDataSource
{
    // To significantly reduce the sample data footprint in your production application, you can set
    // the DISABLE_SAMPLE_DATA conditional compilation constant and disable sample data at runtime.
#if DISABLE_SAMPLE_DATA
	internal class SampleDataSource { }
#else

    public class Item : System.ComponentModel.INotifyPropertyChanged
    {
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }

        private string _Title = string.Empty;
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                if (this._Title != value)
                {
                    this._Title = value;
                    this.OnPropertyChanged("Title");
                }
            }
        }


        private ImageSource _Image = null;
        public ImageSource Image
        {
            get
            {
                return this._Image;
            }

            set
            {
                if (this._Image != value)
                {
                    this._Image = value;
                    this.OnPropertyChanged("Image");
                }
            }
        }

        public void SetImage(Uri baseUri, String path)
        {
            Image = new BitmapImage(new Uri(baseUri, path));
        }

        private string _Link = string.Empty;
        public string Link
        {
            get
            {
                return this._Link;
            }

            set
            {
                if (this._Link != value)
                {
                    this._Link = value;
                    this.OnPropertyChanged("Link");
                }
            }
        }

        private string _Distance = string.Empty;
        public string Distance
        {
            get
            {
                return this._Distance;
            }

            set
            {
                if (this._Distance != value)
                {
                    this._Distance = value;
                    this.OnPropertyChanged("Distance");
                }
            }
        }

        private string _ID = string.Empty;
        public string ID
        {
            get
            {
                return this._ID;
            }

            set
            {
                if (this._ID != value)
                {
                    this._ID = value;
                    this.OnPropertyChanged("ID");
                }
            }
        }
        private string _City = string.Empty;
        public string City
        {
            get
            {
                return this._City;
            }

            set
            {
                if (this._City != value)
                {
                    this._City = value;
                    this.OnPropertyChanged("City");
                }
            }
        }

        private string _Prov = string.Empty;
        public string Prov
        {
            get
            {
                return this._Prov;
            }

            set
            {
                if (this._Prov != value)
                {
                    this._Prov = value;
                    this.OnPropertyChanged("Prov");
                }
            }
        }
    }

    public class GroupInfoList<T> : List<object>
    {

        public object Key { get; set; }


        public new IEnumerator<object> GetEnumerator()
        {
            return (System.Collections.Generic.IEnumerator<object>)base.GetEnumerator();
        }
    }


    public class ResultsData
    {
        public static Dictionary<String, ItemCollection> itemList = new Dictionary<String, ItemCollection>();



        public ResultsData()
        {
        }


        public async void setupItems(String name, String Query, String City)
        {
            Item item;

            Uri _baseUri = new Uri("ms-appx:///");



            if (App.itemListSortedListings.Keys.Contains(name) == false || Query == "Favourites")
            {

                if (Query == "Favourites")
                {
                    App.itemListSortedListings.Add(name, null);

                    for (int x = 0; x < App.favorites.Count(); x++)
                    {
                        if (App.favorites.ElementAt(x).Distance != "")
                        {
                            item = new Item();

                            item.Title = App.favorites.ElementAt(x).Title;
                            item.ID = App.favorites.ElementAt(x).ID;
                            item.SetImage(_baseUri, "SampleData/Images/" + name + ".png");

                            item.Distance = App.favorites.ElementAt(x).Distance;
                            item.City = App.favorites.ElementAt(x).City;
                            item.Prov = App.favorites.ElementAt(x).Prov;

                            if (!itemList.ContainsKey(name))
                                itemList.Add(name, new ItemCollection());

                            itemList[name].Add(item);
                        }
                        else
                            x++;
                    }
                }
                else
                {
                    var client = new HttpClient();

                    //City = "cZ" + loc.Coordinate.Point.Position.Longitude + "," + loc.Coordinate.Point.Position.Latitude;
                    String key = App.getAPIKey().Result;
                    Uri url = new Uri("http://api.sandbox.yellowapi.com/FindBusiness/?what=" + Query + "&where=" + City + "&pgLen=500&pg=1&dist=100&fmt=JSON&lang=en&UID=qwdqqdd&apikey=" + key);

                    string jsonstring = client.GetStringAsync(url).Result;

                    App.itemListSortedListings.Add(name, jsonstring);
                    Businesses.InfoSet values = JsonConvert.DeserializeObject<Businesses.InfoSet>(App.itemListSortedListings[name]);


                    IOrderedEnumerable<Businesses.Listing> sortedList = values.listings.OrderBy(o => o.distance);

                    int Max = 4;
                    int Min = 0;
                    if (sortedList.Count() <= 4)
                        Max = values.summary.currentPage;



                    for (int x = Min; x <= Max; x++)
                    {
                        if (sortedList.ElementAt(x).distance != "")
                        {
                            item = new Item();

                            item.Title = sortedList.ElementAt(x).name;
                            item.ID = sortedList.ElementAt(x).id;
                            item.SetImage(_baseUri, "SampleData/Images/" + name + ".png");

                            item.Distance = App.favorites.ElementAt(x).Distance; 
                            item.City = sortedList.ElementAt(x).address.city;
                            item.Prov = sortedList.ElementAt(x).address.prov;

                            if (!itemList.ContainsKey(name))
                                itemList.Add(name, new ItemCollection());

                            itemList[name].Add(item);
                        }
                        else
                        {
                            Max++;
                        }
                    }
                }
            }

        }


        private async Task<Geoposition> getLocation()
        {
            // Why we are not using async...
            // It is very bad, seems like it is processing the rest of the code before running.
            Geolocator geolocator = new Geolocator();

            Geoposition position = null;
            position = await geolocator.GetGeopositionAsync();

            return position;
        }
        private async Task<String> asynCollect(String url)
        {
            var client = new HttpClient();
            String task = await client.GetStringAsync(new Uri(url));// The freezing happens on this line, if anyone feels like taking o the challange of fixing it. Go ahead :P
            return task;
        }


    }


    public class StoreData
    {
        public static Dictionary<String, ItemCollection> itemList = new Dictionary<String, ItemCollection>();



        public StoreData()
        {
        }


        public async void setupItems(String name, String Query, String City)
        {
            Item item;

            Uri _baseUri = new Uri("ms-appx:///");


            if (App.itemListSortedListings.Keys.Contains(name) == false || Query == "Favourites")
            {

                if (Query == "Favourites")
                {
                    if (!App.itemListSortedListings.ContainsKey(name))
                    App.itemListSortedListings.Add(name, "");


                    if (!itemList.ContainsKey(name))
                        itemList.Add(name, new ItemCollection());
                    else
                        itemList[name] = new ItemCollection();
                    for (int x = 0; x <= App.favorites.Count() - 1; x++)
                    {
                        if (App.favorites.ElementAt(x).Distance != "")
                        {
                            item = new Item();

                            item.Title = App.favorites.ElementAt(x).Title;
                            item.ID = App.favorites.ElementAt(x).ID;
                            item.SetImage(_baseUri, "SampleData/Images/Favourites.png");
                            item.Distance = App.favorites.ElementAt(x).Distance;
                            item.City = App.favorites.ElementAt(x).City;
                            item.Prov = App.favorites.ElementAt(x).Prov;


                            itemList[name].Add(item);
                        }
                        else
                            x++;
                    }
                }
                else
                {
                    var client = new HttpClient();

                    //City = "cZ" + loc.Coordinate.Point.Position.Longitude + "," + loc.Coordinate.Point.Position.Latitude;
                    String key = await App.getAPIKey();
                    Uri url = new Uri("http://api.sandbox.yellowapi.com/FindBusiness/?what=" + Query + "&where=" + City + "&pgLen=500&pg=1&dist=100&fmt=JSON&lang=en&UID=qwdqqdd&apikey=" + key);

                    string jsonstring = client.GetStringAsync(url).Result;

                    App.itemListSortedListings.Add(name, jsonstring);
                    Businesses.InfoSet values = JsonConvert.DeserializeObject<Businesses.InfoSet>(App.itemListSortedListings[name]);


                    IOrderedEnumerable<Businesses.Listing> sortedList = values.listings.OrderBy(o => o.distance);

                    int Max = 4;
                    int Min = 0;
                    if (sortedList.Count() <= 4)
                        Max = values.summary.currentPage;

                    List<String> usedShops = new List<String>();

                    for (int x = Min; x <= Max; x++)
                    {
                        if (sortedList.ElementAt(x).distance != "" && !usedShops.Contains(sortedList.ElementAt(x).id))
                        {
                            item = new Item();
                            usedShops.Add(sortedList.ElementAt(x).id);
                            item.Title = sortedList.ElementAt(x).name;
                            item.ID = sortedList.ElementAt(x).id;
                            item.SetImage(_baseUri, "SampleData/Images/" + name + ".png");
                            item.Distance = sortedList.ElementAt(x).distance + " KM";
                            item.City = sortedList.ElementAt(x).address.city;
                            item.Prov = sortedList.ElementAt(x).address.prov;
                            //System.Diagnostics.Debug.WriteLine("Prov" + item.Prov);

                            if (!itemList.ContainsKey(name))
                                itemList.Add(name, new ItemCollection());

                            itemList[name].Add(item);
                        }
                        else
                        {
                            Max++;
                        }
                    }

                }
            }

        }


        private async Task<Geoposition> getLocation()
        {
            // Why we are not using async...
            // It is very bad, seems like it is processing the rest of the code before running.
            Geolocator geolocator = new Geolocator();

            Geoposition position = null;
            position = await geolocator.GetGeopositionAsync();

            return position;
        }
        private async Task<String> asynCollect(String url)
        {
            var client = new HttpClient();
            String task = await client.GetStringAsync(new Uri(url));// The freezing happens on this line, if anyone feels like taking o the challange of fixing it. Go ahead :P
            return task;
        }


    }


    // Workaround: data binding works best with an enumeration of objects that does not implement IList
    public class ItemCollection : IEnumerable<Object>
    {
        private System.Collections.ObjectModel.ObservableCollection<Item> itemCollection = new System.Collections.ObjectModel.ObservableCollection<Item>();

        public IEnumerator<Object> GetEnumerator()
        {
            return itemCollection.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(Item item)
        {
            itemCollection.Add(item);
        }
    }
#endif
}
