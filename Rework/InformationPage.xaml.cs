﻿using Bing.Maps;
using Expression.Blend.SampleData.SampleDataSource;
using SDKTemplate.Common;
using Newtonsoft.Json;
using SDKTemplate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using Windows.Devices.Geolocation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace SDKTemplate
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class InformationPage : Page
    {
        Item business = null;
        String busID = "";
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        BusinessInfo.InfoSet values = null;
        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public InformationPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            base.OnNavigatedFrom(e);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
            String BusString = e.Parameter as String;
            String[] BusSplitUpData = BusString.Split(';');
            String distance = BusSplitUpData[4];
            String Prov = BusSplitUpData[3];
            String City = BusSplitUpData[2];
            String BusName = Uri.EscapeUriString(BusSplitUpData[1].Split(' ')[0]);
            int BusID = int.Parse(BusSplitUpData[0]);
            Item item = new Item();
            item.Distance = distance;
            item.ID = BusID+"";
            item.Prov = Prov;
            item.City = City;
            busID = BusID+"";
            Uri _baseUri = new Uri("ms-appx:///");
            var client = new HttpClient();

            Uri url = new Uri("http://api.sandbox.yellowapi.com/GetBusinessDetails/?listingId=" + BusID + "&bus-name=" + BusName + "&city=" + City + "&prov=" + Prov + "&fmt=JSON&lang=en&UID=qwdqqdd&apikey=" + App.getAPIKey().Result);
            
            
            string jsonstring = client.GetStringAsync(url).Result;
            values = JsonConvert.DeserializeObject<BusinessInfo.InfoSet>(jsonstring);
            // Here we set the info on the page from the values data list.
            if (values.name != null)
            busTitle.Text = values.name;


            item.Title = values.name;


            business = item;
            if (values.address != null)
            Address.Text = values.address.street + ", " + values.address.city;
            if (values.phones != null)
                if (values.phones.First() != null)
                    PNumber.Text = values.phones.First().dispNum;
            if (values.products.profiles.Count >= 1 && values.products.profiles.ElementAt(0).keywords.OpenHrs != null)
            {
                hoursOpening.Visibility = Windows.UI.Xaml.Visibility.Visible;
                foreach (String dayInfo in values.products.profiles.ElementAt(0).keywords.OpenHrs.ToList()) {
                    String[] splitTime = dayInfo.Split('-');

                    int index = splitTime[0].IndexOf(' ');
                    String split = index >=0 ? splitTime[0].Insert(index, @"[") : splitTime[0];
                    String[] newDateInfo = split.Split('[');
                    // This whole thing was not needed... But guess what. Hehe!

                    String Day = newDateInfo[0];
                    String timeStart = newDateInfo[1];
                    String timeEnd = splitTime[1];
                    TextBlock infoEntry = new TextBlock();
                    infoEntry.Text = dayInfo;
                    infoEntry.FontSize = 20;
                    infoEntry.FontStyle = Windows.UI.Text.FontStyle.Italic;


                    hoursOpenList.Children.Add(infoEntry);
                }
            }
            else
                hoursOpening.Visibility = Windows.UI.Xaml.Visibility.Collapsed;

            
            businessMap.ZoomLevel = 15.0;
            var loc = new Location(float.Parse(values.geoCode.latitude), float.Parse(values.geoCode.longitude));
            SetMapCenter(businessMap, loc);
            Pushpin pushpin = new Pushpin();
            pushpin.Text = "R";
            MapLayer.SetPosition(pushpin, loc);
            businessMap.Children.Add(pushpin);

            var point = mainHubList.pos.Coordinate.Point.Position;

            businessMap.TargetCenter.Longitude = point.Longitude;
            businessMap.TargetCenter.Latitude = point.Latitude;
            //Distance.Text = "http://api.sandbox.yellowapi.com/FindBusiness/?what=Pizza&where=Ottawa&pgLen=400&pg=1&dist=100&fmt=JSON&lang=en&UID=qwdqqdd&apikey=" + App.APIKey;
            Distance.Text = distance;
            //Website.NavigateUri = values.merchantUrl.First();

            if (App.isFaved(business.ID))
                favorite.Label = "Unfavorite";
            else
                favorite.Label = "Favorite";


        }


        public static readonly DependencyProperty MapCenterProperty =
        DependencyProperty.RegisterAttached("MapCenter", typeof(Location), typeof(Map),
        new PropertyMetadata(default(Location), MapCenterChanged));

        private static void MapCenterChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Map map = d as Map;
            Location location = e.NewValue as Location;
            if (map != null && location != null)
            {
                map.Center = location;
            }
        }



        public static void SetMapCenter(UIElement element, Location value)
        {
            element.SetValue(MapCenterProperty, value);
        }

        public static Location GetMapCenter(UIElement element)
        {
            return (Location)element.GetValue(MapCenterProperty);
        }

        private void favoriteToggleRestaurant(object sender, RoutedEventArgs e)
        {
            App.toggleFav(business);
            if (App.isFaved(business.ID)) { 
                favorite.Label = "Unfavorite";
            }
            else{
                favorite.Label = "Favorite";
            }
        }

        private async void navigateToRestaurant(object sender, RoutedEventArgs e)
        {
            string uriToLaunch = uriToLaunch = @"bingmaps://fwrwwf/?cp=" + mainHubList.pos.Coordinate.Point.Position.Latitude + "~" + mainHubList.pos.Coordinate.Point.Position.Longitude + "&lvl=10&where=" + Uri.EscapeUriString(values.address.street + ", " + values.address.city) + "&key=AhtIjLQOTRk5zRdzIV1ukg7DscG1TQY7m9GPyC-PmVURYqtaiKt_YovOcAJd_Ma1";
            var uri = new Uri(uriToLaunch);

            // Launch the URI
            var success = await Windows.System.Launcher.LaunchUriAsync(uri);
        }
        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.



        #endregion
    }
}
