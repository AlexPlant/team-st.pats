﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SDKTemplate
{
    class BusinessInfo
    {
        public class Address
        {
            public string street { get; set; }
            public string city { get; set; }
            public string prov { get; set; }
            public string pcode { get; set; }
        }

        public class Phone
        {
            public string npa { get; set; }
            public string nxx { get; set; }
            public string num { get; set; }
            public string dispNum { get; set; }
            public string type { get; set; }
        }

        public class GeoCode
        {
            public string latitude { get; set; }
            public string longitude { get; set; }
        }

        public class Category
        {
            public string name { get; set; }
            public bool isSensitive { get; set; }
        }

        public class Video
        {
            public string universalUrl { get; set; }
            public string url { get; set; }
            public string thmbUrl { get; set; }
        }

        public class Keywords
        {
            public List<string> OpenHrs { get; set; }
            public List<string> LangSpk { get; set; }
            public List<string> ProdServ { get; set; }
            public List<string> Special { get; set; }
            public List<string> BrndCrrd { get; set; }
        }

        public class Profile
        {
            public string lang { get; set; }
            public Keywords keywords { get; set; }
        }

        public class Products
        {
            public List<string> webUrl { get; set; }
            public List<object> dispAd { get; set; }
            public List<Video> videos { get; set; }
            public List<object> photos { get; set; }
            public List<Profile> profiles { get; set; }
        }

        public class Logos
        {
            public string EN { get; set; }
            public string FR { get; set; }
        }

        public class InfoSet
        {
            public string id { get; set; }
            public string name { get; set; }
            public Address address { get; set; }
            public List<Phone> phones { get; set; }
            public GeoCode geoCode { get; set; }
            public List<Category> categories { get; set; }
            public Products products { get; set; }
            public Logos logos { get; set; }
            public string merchantUrl { get; set; }
        }
    }
}
