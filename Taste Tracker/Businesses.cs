﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taste_Tracker
{
    class Businesses
    {
        public class Summary
        {
            public string what { get; set; }
            public string where { get; set; }
            public int firstListing { get; set; }
            public int lastListing { get; set; }
            public int totalListings { get; set; }
            public int pageCount { get; set; }
            public int currentPage { get; set; }
            public int listingsPerPage { get; set; }
            public string Prov { get; set; }
        }

        public class Address
        {
            public string street { get; set; }
            public string city { get; set; }
            public string prov { get; set; }
            public string pcode { get; set; }
        }

        public class GeoCode
        {
            public string latitude { get; set; }
            public string longitude { get; set; }
        }

        public class Video
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class Photo
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class Profile
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class DspAd
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class Logo
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class Url
        {
            public bool avail { get; set; }
            public bool inMkt { get; set; }
        }

        public class Content
        {
            public Video Video { get; set; }
            public Photo Photo { get; set; }
            public Profile Profile { get; set; }
            public DspAd DspAd { get; set; }
            public Logo Logo { get; set; }
            public Url Url { get; set; }
        }

        public class Listing
        {
            public string id { get; set; }
            public string name { get; set; }
            public Address address { get; set; }
            public GeoCode geoCode { get; set; }
            public string distance { get; set; }
            public string parentId { get; set; }
            public bool isParent { get; set; }
            public Content content { get; set; }
        }

        public class InfoSet
        {
            public Summary summary { get; set; }
            public List<Listing> listings { get; set; }
        }
    }
}
