﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Taste_Tracker
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    public sealed partial class MainPage : Page
    {
        public String APIKey = "d4h9eb4cppjtkaw5kmkjqxfs";

        public MainPage()
        {
            this.InitializeComponent();
            LoadMaininfo();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(DemoPage));
        }

        private void SearchBoxEventsQuerySubmitted(Windows.UI.Xaml.Controls.SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            this.Frame.Navigate(typeof(SearchResult), args.QueryText);
        }



        private async void LoadMaininfo()
        {

            setUpNameTags("Pizza");
            await Task.Delay(1000);
            setUpNameTags("Chinese");
            await Task.Delay(1000);
            setUpNameTags("Sushi");
            await Task.Delay(1000);
            setUpNameTags("Burgers");

        }

        private async void setUpNameTags(string catagory)
        {
            Task<Businesses.InfoSet> infoPizza = getInfoSet(catagory, 5, "Ottawa");
            await Task.Delay(1000);
            int Max = 0;
            if (infoPizza.Result.listings.Count <= 5)
                Max = infoPizza.Result.listings.Count - 1;
            else
                Max = 4;

            for (int x = 0; x <= Max; x++)
            {
                TextBlock bl = (TextBlock)this.FindName(catagory +"Name" + (x + 1));
                bl.Text = infoPizza.Result.listings[x].name;
            }


        }



        private async Task<Businesses.InfoSet> getInfoSet(string searchQuery, int results, string City)
        {
            var client = new HttpClient();
            var uri = new Uri("http://api.sandbox.yellowapi.com/FindBusiness/?what=" + searchQuery + "&where=" + City + "&pgLen=" + results + "&pg=1&dist=100&fmt=JSON&lang=en&UID=qwdqqdd&apikey=d4h9eb4cppjtkaw5kmkjqxfs");
            string jsonstring = await client.GetStringAsync(uri);
            Businesses.InfoSet values = JsonConvert.DeserializeObject<Businesses.InfoSet>(jsonstring);
            return values;
        }


    }
}
