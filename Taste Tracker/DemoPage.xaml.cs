﻿using Taste_Tracker.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Net.Http;
using Newtonsoft.Json;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace Taste_Tracker
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class DemoPage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public DemoPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }



        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var client = new HttpClient();
            var uri = new Uri("http://api.sandbox.yellowapi.com/FindBusiness/?what=Restaurants&where=Toronto&pgLen=5&pg=1&dist=1&fmt=JSON&lang=en&UID=qwdqqdd&apikey=d4h9eb4cppjtkaw5kmkjqxfs");
            string jsonstring = await client.GetStringAsync(uri);
            Businesses.InfoSet values = JsonConvert.DeserializeObject<Businesses.InfoSet>(jsonstring);
            String name = values.listings[0].name;
            String city = values.listings[0].address.city;
            String street = values.listings[0].address.street;

            pageTitle.Text = name;

            if (values.listings[0].address != null)
            {
                if (city != null)
                    City.Text = city;
                else
                    City.Text = "Not Available";

                if (street != null)
                    Street.Text = street;
                else
                    Street.Text = "Not Available";
            }

            PhoneNumber.Text = "Loading...";
            await Task.Delay(1000);
            grabBusinessInfo(values.listings[0].id, values.listings[0].name, values.listings[0].address.city, values.listings[0].address.prov);
        }

        async void grabBusinessInfo(String companyID, String companyName, String city, String Prov)
        {

            var client = new HttpClient();
            String[] name = companyName.Split(' ');
            var uri = new Uri("http://api.sandbox.yellowapi.com/GetBusinessDetails/?listingId=" + companyID + "&bus-name=" + name[0] + "&city=" + city + "&prov=" + Prov + "&fmt=JSON&lang=en&UID=qwdqqdd&apikey=d4h9eb4cppjtkaw5kmkjqxfs");
            string jsonstring = await client.GetStringAsync(uri);
            BusinessInfo.InfoSet values = JsonConvert.DeserializeObject<BusinessInfo.InfoSet>(jsonstring);
            if (values.phones.Count >= 1)
                this.PhoneNumber.Text = values.phones[0].dispNum;
            else
                this.PhoneNumber.Text = "Not Available";
        }


        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

    }
}
